<?php

namespace Drupal\graphql_config;

use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class ConfigPermissions.
 *
 * @package Drupal\graphql_config
 */
class ConfigPermissions {

  use StringTranslationTrait;

  /**
   * Build permissions for the config.
   */
  public function permissions() {
    $permissions = [];

    if ($configs = Settings::get('graphql_config')) {
      foreach ($configs as $name) {
        $permissions['allow access to ' . $name . ' config'] = [
          'title' => $this->t('Allow access to %name config', ['%name' => $name]),
        ];
      }
    }

    return $permissions;
  }
}
