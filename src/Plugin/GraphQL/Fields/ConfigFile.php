<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_file",
 *   secure = true,
 *   type = "ConfigFile",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigFileDeriver"
 * )
 */
class ConfigFile extends ConfigFieldBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $definition = $this->getPluginDefinition();
    if ($value instanceof Config) {
      $target_id = $value->get($definition['config_path']);
    }
    else {
      // TODO: this may  be an issue as I may need just the last name.
      $target_id = NestedArray::getValue($value, $definition['config_path']);
    }

    yield $this->entityTypeManager->getStorage('file')->load($target_id);
  }

}
