<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigTextFormat.
 *
 * @GraphQLField(
 *   id = "config_text_format",
 *   secure = true,
 *   type = "ConfigTextFormat",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigTextFormatDeriver"
 * )
 */
class ConfigTextFormat extends ConfigFieldBase {

}
