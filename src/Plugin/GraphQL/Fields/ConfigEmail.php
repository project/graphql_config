<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_email",
 *   secure = true,
 *   type = "Email",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigEmailDeriver"
 * )
 */
class ConfigEmail extends ConfigFieldBase {

}
