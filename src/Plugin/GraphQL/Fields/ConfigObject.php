<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigObject.
 *
 * @GraphQLField(
 *   id = "config_object",
 *   secure = true,
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigObjectDeriver"
 * )
 */
class ConfigObject extends ConfigFieldBase {

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, AccountProxyInterface $account) {
    $this->account = $account;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($this->account->hasPermission('allow access to ' . $this->getPluginDefinition()['config_name'] . ' config')) {
      $config = $this->configFactory->get($this->getPluginDefinition()['config_name']);

      yield new CacheableValue($config, [$config]);
    }
  }

}
