<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_image",
 *   secure = true,
 *   type = "ConfigImage",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigImageDeriver"
 * )
 */
class ConfigImage extends ConfigFile {

}
