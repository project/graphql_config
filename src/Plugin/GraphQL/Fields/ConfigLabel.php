<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_label",
 *   secure = true,
 *   type = "String",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigLabelDeriver"
 * )
 */
class ConfigLabel extends ConfigFieldBase {

}
