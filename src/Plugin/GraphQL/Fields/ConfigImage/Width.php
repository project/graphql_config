<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigImage;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the image field derivative (image style).
 *
 * @GraphQLField(
 *   id = "config_image_width",
 *   secure = true,
 *   name = "width",
 *   parents = { "ConfigImage" },
 *   type = "Int",
 * )
 */
class Width extends ConfigFieldBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('config.factory'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    /** @var \Drupal\file\Entity\File $value */
    if ($value->access('view')) {

      list($width) = @getimagesize($this->fileSystem->realpath($value->getFileUri()));

      yield new CacheableValue((int) $width, [$value]);
    }
  }

}
