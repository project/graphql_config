<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigImage;

use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFile\Url as FileUrl;

/**
 * Class Url.
 *
 * @GraphQLField(
 *   id = "config_image_url",
 *   name = "url",
 *   parents = { "ConfigImage" },
 *   secure = true,
 *   type = "String"
 * )
 */
class Url extends FileUrl {

}
