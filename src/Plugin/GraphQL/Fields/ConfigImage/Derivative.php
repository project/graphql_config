<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigImage;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\image\Entity\ImageStyle;

/**
 * Retrieve the image field derivative (image style).
 *
 * @GraphQLField(
 *   id = "config_image_derivative",
 *   secure = true,
 *   name = "derivative",
 *   parents = { "ConfigImage" },
 *   type = "ImageResource",
 *   arguments = {
 *     "style" = "ImageStyleId!"
 *   },
 * )
 */
class Derivative extends ConfigFieldBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem, ImageFactory $imageFactory) {
    $this->fileSystem = $fileSystem;
    $this->imageFactory = $imageFactory;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('image.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    /** @var \Drupal\file\Entity\File $value */
    if ($value->access('view') && $style = ImageStyle::load($args['style'])) {

      list($width, $height) = @getimagesize($this->fileSystem->realpath($value->getFileUri()));
      // Determine the dimensions of the styled image.
      $dimensions = [
        'width' => $width,
        'height' => $height,
      ];

      $style->transformDimensions($dimensions, $value->getFileUri());

      yield new CacheableValue([
        'url' => $style->buildUrl($value->getFileUri()),
        'width' => $dimensions['width'],
        'height' => $dimensions['height'],
      ], [$value, $style]);
    }
  }

}
