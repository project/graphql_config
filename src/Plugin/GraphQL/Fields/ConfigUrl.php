<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_url",
 *   secure = true,
 *   type = "String",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigUrlDeriver"
 * )
 */
class ConfigUrl extends ConfigFieldBase {

}
