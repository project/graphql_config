<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFile;

use Drupal\file\FileInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class Url.
 *
 * @GraphQLField(
 *   id = "config_file_url",
 *   name = "url",
 *   parents = { "ConfigFile" },
 *   secure = true,
 *   type = "String"
 * )
 */
class Url extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof FileInterface) {
      yield file_create_url($value->getFileUri());
    }
  }

}
