<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_text",
 *   secure = true,
 *   type = "String",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigTextDeriver"
 * )
 */
class ConfigText extends ConfigFieldBase {

}
