<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_string",
 *   secure = true,
 *   type = "String",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigStringDeriver"
 * )
 */
class ConfigString extends ConfigFieldBase {

}
