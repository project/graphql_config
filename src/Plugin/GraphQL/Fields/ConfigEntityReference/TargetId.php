<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigEntityReference;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class TargetId.
 *
 * @GraphQLField(
 *   id = "config_entity_reference_target_id",
 *   name = "target_id",
 *   parents = { "ConfigEntityReference" },
 *   secure = true,
 *   type = "String"
 * )
 */
class TargetId extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['target_id'];
  }

}
