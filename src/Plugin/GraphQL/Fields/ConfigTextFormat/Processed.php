<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigTextFormat;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigTextFormat.
 *
 * @GraphQLField(
 *   id = "config_text_format_processed",
 *   name = "processed",
 *   parents = { "ConfigTextFormat" },
 *   secure = true,
 *   type = "String"
 * )
 */
class Processed extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, RendererInterface $renderer) {
    $this->renderer = $renderer;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $element = [
      '#type' => 'processed_text',
      '#text' => $value['value'],
      '#format' => $value['format'],
    ];
    yield $this->renderer->renderRoot($element);
  }

}
