<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigTextFormat;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class ConfigTextFormat.
 *
 * @GraphQLField(
 *   id = "config_text_format_format",
 *   name = "format",
 *   parents = { "ConfigTextFormat" },
 *   secure = true,
 *   type = "String"
 * )
 */
class Format extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['format'];
  }

}
