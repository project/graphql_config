<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Config;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class ConfigString.
 *
 * @GraphQLField(
 *   id = "config_entity_reference",
 *   secure = true,
 *   type = "ConfigEntityReference",
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Fields\ConfigEntityReferenceDeriver"
 * )
 */
class ConfigEntityReference extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $definition = $this->getPluginDefinition();
    if ($value instanceof Config) {
      $target_id = $value->get($definition['config_path']);
    }
    else {
      // TODO: this may  be an issue as I may need just the last name.
      $target_id = NestedArray::getValue($value, $definition['config_path']);
    }

    yield [
      'target_entity' => $definition["target_entity"],
      'target_id' => $target_id,
    ];
  }

}
