<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigObject;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_config\Plugin\GraphQL\Fields\ConfigFieldBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class Id.
 *
 * @GraphQLField(
 *   id = "config_object_id",
 *   name = "id",
 *   secure = true,
 *   type = "String",
 *   parents = { "configObject" }
 * )
 */
class Id extends ConfigFieldBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value->getName();
  }

}
