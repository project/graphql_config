<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Class ConfigObjectType.
 *
 * @GraphQLType(
 *   id = "config_text_format",
 *   name = "ConfigTextFormat",
 *   description = @Translation("Wrapper type for the config_text_format")
 * )
 */
class ConfigTextFormat extends TypePluginBase {

}
