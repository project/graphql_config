<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Class ConfigObjectType.
 *
 * @GraphQLType(
 *   id = "config_image",
 *   name = "ConfigImage",
 *   description = @Translation("Wrapper type for the config_image")
 * )
 */
class ConfigImage extends TypePluginBase {

}
