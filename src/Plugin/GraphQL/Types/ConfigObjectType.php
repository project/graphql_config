<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Class ConfigObjectType.
 *
 * @GraphQLType(
 *   id = "config_object",
 *   name = "configObject",
 *   description = @Translation("Wrapper type for the config_object")
 * )
 */
class ConfigObjectType extends TypePluginBase {

}
