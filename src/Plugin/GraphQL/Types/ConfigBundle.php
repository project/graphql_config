<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\Core\Config\Config;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class ConfigBundle.
 *
 * @GraphQLType(
 *   id = "config_bundle",
 *   schema_cache_tags = { "config_bundle" },
 *   deriver = "Drupal\graphql_config\Plugin\Deriver\Types\ConfigBundleDeriver"
 * )
 */
class ConfigBundle extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {

    if ($object instanceof Config) {
      /** @var \Drupal\Core\Config\Config $object */
      $definition = $this->getPluginDefinition();
      return $definition['config_name'] == $object->getName();
    }

    return FALSE;
  }

}
