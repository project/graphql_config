<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Class ConfigObjectType.
 *
 * @GraphQLType(
 *   id = "config_entity_reference",
 *   name = "ConfigEntityReference",
 *   description = @Translation("Wrapper type for the config_entity_reference")
 * )
 */
class ConfigEntityReference extends TypePluginBase {

}
