<?php

namespace Drupal\graphql_config\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Class ConfigObjectType.
 *
 * @GraphQLType(
 *   id = "config_file",
 *   name = "ConfigFile",
 *   description = @Translation("Wrapper type for the config_file")
 * )
 */
class ConfigFile extends TypePluginBase {

}
