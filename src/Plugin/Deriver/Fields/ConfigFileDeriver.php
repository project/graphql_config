<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

/**
 * Class ConfigStringDeriver.
 *
 * @package Drupal\graphql_config\Plugin\Deriver\Fields
 */
class ConfigFileDeriver extends ConfigDeriverFieldBase {

  const CONFIGTYPE = "entity_reference_file";

}
