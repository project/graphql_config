<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

use Drupal\graphql_config\Plugin\Deriver\ConfigDeriverBase;

/**
 * Class ConfigDeriverBase.
 *
 * @package Drupal\graphql_config\Plugins\Deriver
 */
abstract class ConfigDeriverFieldBase extends ConfigDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = $this->getConfigDefinition(static::CONFIGTYPE);

    foreach ($definitions as $definition) {
      $derivative = [
        'name' => $definition['field_name'],
        'parents' => [array_pop($definition['parent_names'])],
        'description' => $definition['label'] . ' configuration',
        'config_name' => $definition['name'],
        'config_path' => $definition['path'],
      ] + $base_plugin_definition;

      $this->derivatives['config-' . $definition['name'] . '-' . $definition['path']] = $derivative;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
