<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

/**
 * Class ConfigTextFormatDeriver.
 *
 * @package Drupal\graphql_config\Plugin\Deriver\Fields
 */
class ConfigTextFormatDeriver extends ConfigDeriverFieldBase {

  const CONFIGTYPE = "text_format";

}
