<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

/**
 * Class ConfigStringDeriver.
 *
 * @package Drupal\graphql_config\Plugin\Deriver\Fields
 */
class ConfigEntityReferenceDeriver extends ConfigDeriverFieldBase {

  const CONFIGTYPE = "entity_reference";

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = $this->getConfigDefinition(static::CONFIGTYPE);

    foreach ($definitions as $definition) {
      if (empty($definition['definition']['target_entity'])) {
        throw new \Exception("No target entity set for configuration");
      }
      $target_entity = $definition['definition']['target_entity'];

      $derivative = [
        'name' => $definition['field_name'],
        'parents' => [array_pop($definition['parent_names'])],
        'description' => $definition['label'] . ' configuration',
        'config_name' => $definition['name'],
        'config_path' => $definition['path'],
        'target_entity' => $target_entity,
      ] + $base_plugin_definition;

      $this->derivatives['config-' . $definition['name'] . '-' . $definition['path']] = $derivative;
    }

    return $this->derivatives;
  }

}
