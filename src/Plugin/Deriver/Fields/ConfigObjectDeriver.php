<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

/**
 * Class ConfigObjectDeriver.
 *
 * @package Drupal\graphql_config\Plugin\Deriver\Fields
 */
class ConfigObjectDeriver extends ConfigDeriverFieldBase {

  const CONFIGTYPE = "config_object";

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = $this->getConfigDefinition(static::CONFIGTYPE);

    foreach ($definitions as $definition) {
      $derivative = [
        'name' => $definition['field_name'],
        'description' => $definition['label'] . ' Configuration',
        'config_name' => $definition['name'],
        'type' => $definition['field_name'],
      ] + $base_plugin_definition;

      $this->derivatives['config_object-' . $definition['name']] = $derivative;
    }

    return $this->derivatives;
  }

}
