<?php

namespace Drupal\graphql_config\Plugin\Deriver\Fields;

/**
 * Class ConfigStringDeriver.
 *
 * @package Drupal\graphql_config\Plugin\Deriver\Fields
 */
class ConfigLabelDeriver extends ConfigDeriverFieldBase {

  const CONFIGTYPE = "label";

}
