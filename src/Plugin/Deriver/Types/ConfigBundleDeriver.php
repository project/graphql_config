<?php

namespace Drupal\graphql_config\Plugin\Deriver\Types;

use Drupal\graphql_config\Plugin\Deriver\ConfigDeriverBase;

/**
 * Config Bundle Deriver.
 */
class ConfigBundleDeriver extends ConfigDeriverBase {

  const CONFIGTYPE = "config_object";

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $definitions = $this->getConfigDefinition(static::CONFIGTYPE);

    foreach ($definitions as $definition) {
      $derivative = [
        'name' => $definition['field_name'],
        'description' => $definition['label'] . ' Configuration',
        'type' => 'config_object-' . $definition['name'],
        'config_name' => $definition['name'],
      ] + $base_plugin_definition;

      $this->derivatives['config_object-' . $definition['name']] = $derivative;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
