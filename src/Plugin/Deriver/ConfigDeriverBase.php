<?php

namespace Drupal\graphql_config\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Schema\TypedConfigInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\graphql\Utility\StringHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ConfigDeriverBase.
 */
abstract class ConfigDeriverBase extends DeriverBase implements ContainerDeriverInterface {

  const CACHE_NAME = 'graphql_config_index';

  const IGNORENESTINGTYPES = ['text_format', 'mapping'];

  /**
   * Typed Config Manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Configuration index.
   *
   * @var array
   */
  private $configIndex;

  /**
   * {@inheritdoc}
   */
  public function __construct(TypedConfigManagerInterface $typedConfigManager, CacheBackendInterface $cache) {
    $this->typedConfigManager = $typedConfigManager;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.typed'),
      $container->get('cache.default')
    );
  }

  /**
   * Get the config index for the config type.
   *
   * @param string $type
   *   Type of configuration wanted.
   *
   * @return mixed|null
   */
  protected function getConfigDefinition($type) {
    $configDefinitions = $this->getConfigDefinitions();

    return isset($configDefinitions[$type]) ? $configDefinitions[$type] : [];
  }

  /**
   * Get all config index.
   */
  protected function getConfigDefinitions() {
    if (empty($this->configIndex)) {
      $cache = $this->cache->get(static::CACHE_NAME);
      if (empty($cache->data)) {
        $this->buildConfigDefinitions();
        $this->cache->set(static::CACHE_NAME, $this->configIndex);
      }
      else {
        $this->configIndex = $cache->data;
      }
    }

    return $this->configIndex;
  }

  /**
   * Build configuration index.
   */
  protected function buildConfigDefinitions() {
    if ($configs = Settings::get('graphql_config')) {
      foreach ($configs as $config) {
        $config_schema = $this->typedConfigManager->get($config);
        $this->buildConfigDefinition($config_schema, 'config_object');
      }
    }
  }

  /**
   * Build the configuration index for all types.
   */
  protected function buildConfigDefinition(TypedDataInterface $config, $type = NULL) {
    if (!isset($type)) {
      $type = $config->getDataDefinition()->getDataType();
    }

    $parents = $this->getConfigParents($config);
    $parent_names = $this->getConfigParentNames($config);
    $data_definition = $config->getDataDefinition();
    $item = [
      'id' => $this->getConfigId($config),
      'path' => $config->getPropertyPath(),
      'label' => $config->getDataDefinition()->getLabel(),
      'name' => $this->getConfigName($config),
      'field_name' => array_pop($parent_names),
      'parents' => $parents,
      'definition' => $data_definition->toArray(),
    ];

    if ($config->getParent()) {
      $item['parent_names'] = $parent_names;
    }

    $this->configIndex[$type][$item['id']] = $item;

    if ($config instanceof TypedConfigInterface && !in_array($type, static::IGNORENESTINGTYPES)) {
      foreach ($config->getElements() as $element) {
        $this->buildConfigDefinition($element);
      }
    }
  }

  /**
   * Get the id of the current config.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $config
   *   Config object.
   *
   * @return mixed
   */
  protected function getConfigId(TypedDataInterface $config) {
    $name = $this->getConfigParents($config);
    array_unshift($name, $this->getConfigName($config));
    return str_replace('.', '_', implode('_', $name));
  }

  /**
   * Get the configuration id.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $config
   *   Config Object.
   *
   * @return string
   */
  protected function getConfigName(TypedDataInterface $config) {
    return $config->getRoot()->getDataDefinition()->getDataType();
  }

  /**
   * Get the parents of the the current config object.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $config
   *   Config Object.
   *
   * @return array
   */
  protected function getConfigParents(TypedDataInterface $config) {
    $parents = [];
    if ($parent = $config->getParent()) {
      $parents = $this->getConfigParents($parent);
    }

    if ($name = $config->getName()) {
      $parents[] = $name;
    }

    return $parents;
  }

  /**
   * Get the configuration parent names.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $config
   *   Config Object.
   *
   * @return array
   */
  protected function getConfigParentNames(TypedDataInterface $config) {
    $parents = [];
    if ($parent = $config->getParent()) {
      $parents = $this->getConfigParentNames($parent);
    }

    if (!$parent) {
      $parents[] = StringHelper::propCase('config', $this->getConfigName($config));
    }
    elseif ($name = $config->getName()) {
      $parents[] = StringHelper::propCase(str_replace('.', '_', $name));
    }

    return $parents;
  }

}
