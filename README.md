# GraphQL Config

This exports Config Objects to GraphQL so that it can be accessed remotely.

## Configuration.

Allowing all config to be exported is extremely dangerous. There is a lot of config items which hold very sensitive data. So be careful in what config items are exported.

```
$settings['graphql_config'] = ['system.site'];
```

This will expose the `system.site` config item as an object which can be queried and the items will return structured in the correct format.

## Config schema.

To help with structuring the there have been a few new config schema types that been added.

* entity_reference - Adds an entity reference data type to all the connecting of the config to external content. The scheme will also need to specify the `target_entity` which set which entity type it references.
* entity_reference_file - Add reference to a file which can retrieve the url of this file.
* entity_reference_image - Adds reference to an image which works like an entity image field and allows the returns of a derivative so the image can be styled as needed.  
